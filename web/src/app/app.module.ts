import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CommonService} from './services/common.service';
import {HttpClientModule} from '@angular/common/http';
import {InlineEditorModule} from '@qontu/ngx-inline-editor';
import { FormsModule } from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        FormsModule,
        InlineEditorModule,
        BrowserModule,
        HttpClientModule,
        MatSidenavModule,
        BrowserAnimationsModule
    ],
    providers: [CommonService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
