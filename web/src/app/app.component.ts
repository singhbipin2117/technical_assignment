import {Component, OnInit, ViewChild} from '@angular/core';
import {CommonService} from "./services/common.service";
import { InlineEditorComponent } from '@qontu/ngx-inline-editor';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'app';
    @ViewChild('InlineEditorComponent') private _child: InlineEditorComponent;
    getData : Array<
        {
            Category : String,
            Range_Brand : String,
            Brand : String,
            brand_id : String,
            0:{},
            1:{},
            2:{},
            3:{},
            4:{},
            5:{},
            6:{},
            7:{},
            8:{},
            9:{},
            10:{},
            11:{},
            12:{},
        }
        >;
    versionData:any;
    brand:any;
    url:String = "http://ajmaurya.com/bipin/technical_assignment";
    // url:String = "http://localhost/bipin/technical_assignment";
    constructor(public commonService: CommonService) {
    }

    ngOnInit() {
        this.get_data();
    }

    get_data() {
        const url = this.url+"/api/TechnicalAssignment/getdata";
        const data = "";
        this.commonService.post_data(url, data).subscribe((response: any) => {
            if (response.status == 0) {
                this.getData = response.data;
            }


        })
    }

    saveEditable($ev,data){
        const url = this.url+"/api/TechnicalAssignment/saveversion";
        this.commonService.post_data(url, data).subscribe((response: any) => {
            this.getData = response.data;
            this.getVersion(data.data_id);
        })

    }

    ConvertString(value1,value2,value3,value4,value5,value6){
        return parseInt(value1) +parseInt(value2)+parseInt(value3)+parseInt(value4) +parseInt(value5)+parseInt(value6)
    }

    ConvertStringTotal(value1,value2,value3){
        return parseInt(value1) +parseInt(value2)+parseInt(value3)
    }

    myHandleError(){
        this._child.edit({ editing: true, focus: true, select: true });
    }
    getVersion(id){
        var data = {"id": id};
        const url = this.url+"/api/TechnicalAssignment/getversion";
        this.commonService.post_data(url, data).subscribe((response: any) => {
            this.versionData = response.data;
            this.brand = response.brand[0];
        })
    }
}
