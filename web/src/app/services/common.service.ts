import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/observable/throw';

@Injectable()
export class CommonService {
    loading: any;
    loggedin_status = new Subject<any>();
    header_status = new Subject<any>();
    userinfo = new Subject<any>();
    driveinfo = new Subject<any>();


    constructor(public http: HttpClient) {

    }

    post_data(url, postdata) {
        const headerJson = {
             'Content-Type': 'multipart/form-data'
        }
        const headers = new HttpHeaders(headerJson);
        const options = {headers: headers};

        return this.http.post(url, postdata, options).map(
            res => {
                return res;
            }
        ).catch((err: HttpErrorResponse) => {
            if (err.status === 400) {
                //this.setStatus(0);
            }
            const errorObj = err.error;
            return Observable.throw(errorObj);
        })
    }

    get_data(url, show_loader = true) {
        // console.log(localStorage.getItem('token'));
        const headerJson = {
            'Content-Type': 'multipart/form-data'
        }
        const headers = new HttpHeaders(headerJson);
        const options = {headers: headers};
        return this.http.get(url, options).map(
            res => {
                return res;
            }
        ).catch((err: HttpErrorResponse) => {
            if (err.status === 400) {
                //this.setStatus(0);
            }
            const errorObj = err.error;
            return Observable.throw(errorObj);
        });
    }

    setStatus(status: number) {
        this.loggedin_status.next({status: status});
    }

    clearStatus() {
        this.loggedin_status.next();
    }

    getStatus(): Observable<any> {
        return this.loggedin_status.asObservable();
    }


    /**
     *
     * @param status
     */
    setHeader(status: number) {
        this.header_status.next({status: status});
    }


    getHeader(): Observable<any> {
        return this.header_status.asObservable();
    }

    setUserInfo(userinfo) {
        console.log(userinfo);
        this.userinfo.next(userinfo);
    }

    getUserInfo(): Observable<any> {
        return this.userinfo.asObservable();
    }

    setDeriveDetail(data) {
        this.driveinfo.next(data);
    }

    getDriveDetail(): Observable<any> {
        return this.driveinfo.asObservable();
    }


    upload_file(url, postdata) {
        const headersParam = new HttpHeaders();
        const options = {headers: headersParam};

        return this.http.post(url, postdata, options).map(
            res => {
                return res;
            }
        ).catch((err: HttpErrorResponse) => {
                if (err.status === 400) {
                    //this.setStatus(0);
                }
                const errorObj = err.error;
                return Observable.throw(errorObj);
            }
        );
    }


    setLoginCredential(response) {
        console.log(response);
        window.localStorage.setItem('credentials', JSON.stringify(response));
        return true;
    }

    getLoginCredential() {
        const credentials = JSON.parse(window.localStorage.getItem('credentials'));
        return credentials;
    }
}