<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
require APPPATH . 'libraries/REST_Controller.php';

class TechnicalAssignment extends REST_Controller
{
    public $columnKeys;

    public function __construct($config = 'rest')
    {
        parent::__construct();
    }

    /**
     * Get data function
     */
    public function getdata_post()
    {
        $finalResult = $this->finalResult();
        $message = array("status" => 0, "data" => $finalResult);
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    /**
     *
     * Private _initializeArray function
     *
     * @param $keys array
     * @return array
     */
    function _initializeArray($keys)
    {
        $blankArray = array();
        for ($i = 0; $i <= 1; $i++) {
            foreach ($keys as $val) {
                array_push($blankArray, array($val['name'] => 0));
            }
            array_push($blankArray, array('Total' => 0)); // Adding array in $blankArray
        }
        return $blankArray;
    }

    function _additionValue($val, $blankArray)
    {
        for ($i = 0; $i <= 13; $i++) {
            $keyName = array_keys($val[$i]);
            $blankArray[$i][$keyName[0]] = $blankArray[$i][$keyName[0]] + $val[$i][$keyName[0]]['value'];
        }
        return $blankArray;
    }

    /**
     * saveversion post data function
     */
    function saveversion_post()
    {
        $_POST = json_decode(file_get_contents("php://input"), true); //geting http data from php:..input to php post variable

        $dataValue = $this->input->post('value');
        $data_id = $this->input->post('data_id');
        // select query
        $selectPreviouRecord = $this->api_model->select('data', 'value', array('id' => $this->input->post('data_id')), null, null, true, null);
        if($selectPreviouRecord){
            $data = array(
                'value' => $selectPreviouRecord['value'],
                'data_id' => $this->input->post('data_id'),
            );
            //insert versioning data into data_verion table
            $this->api_model->insert_noid('data_version', $data);
        }
        //Update data to new record
        $updateQuery = "UPDATE data set value = $dataValue where id = $data_id";
        $this->api_model->custom_query_update($updateQuery);
        $finalResult = $this->finalResult();
        $message = array("status" => 0, "data" => $finalResult);
        //sending response to browser
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    /**
     *
     * Creating formatted array
     *
     * @return array
     */
    private function finalResult()
    {
        $gettingKeys = "SELECT name FROM `advertisement_media`";
        $gettingKeysResult = $this->api_model->custom_query($gettingKeys);
        $sql = 'select root.name  as Category, down1.name as Range_Brand, down2.name as Brand, down2.id as brand_id from categories as root left outer join categories as down1  on down1.parentid = root.id left outer join categories as down2 on down2.parentid = down1.id where root.parentid is null order by brand_id';

        $brand = $this->api_model->custom_query($sql);
        if (!empty($brand)) {
            $additionArray = $this->_initializeArray($gettingKeysResult);
            $i = 1;
            foreach ($brand as $items) {
                $valueAddtion = 0;
                $brand_id = $items['brand_id'];
                $queryValue = "SELECT `data`.`id` as data_id, `data`.`value`, `data`.`timestamp`, `categories`.`name`,`categories`.`id`, `advertisement_media`.`name`, `advertisement_media`.`id` as advertiement_media FROM `data` LEFT JOIN `advertisement_media` ON `data`.`advertisement_media` = `advertisement_media`.`id` LEFT JOIN `categories` ON `data`.`categories` = `categories`.`id` WHERE `data`.`categories` = $brand_id ORDER BY data_id ASC";
                $queryValueResult = $this->api_model->custom_query($queryValue);
                foreach ($queryValueResult as $value) {
                    $items[][$value['name']] = array('value' => $value['value'],
                        'timestamp' => $value['timestamp'],
                        'id' => $value['id'],
                        'data_id' => $value['data_id'],
                        'advertiement_media' => $value['advertiement_media']);
                    $valueAddtion = $valueAddtion + $value['value'];
                    if ($value['name'] == 'BTL') {
                        $items[]['Total'] = array('value' => $valueAddtion);
                        $valueAddtion = 0;
                    }
                }
                $additionArray = $this->_additionValue($items, $additionArray);
                $finalResult[] = $items;
                if ($i % 3 == 0) {
                    $finalResult[]['Total'] = array(
                        array("TV" => $additionArray[0]['TV']),
                        array("Print" => $additionArray[1]['Print']),
                        array("Radio" => $additionArray[2]['Radio']),
                        array("Digital" => $additionArray[3]['Digital']),
                        array("OOH" => $additionArray[4]['OOH']),
                        array("BTL" => $additionArray[5]['BTL']),
                        array("Total" => $additionArray[6]['Total']),
                        array("TV" => $additionArray[7]['TV']),
                        array("Print" => $additionArray[8]['Print']),
                        array("Radio" => $additionArray[9]['Radio']),
                        array("Digital" => $additionArray[10]['Digital']),
                        array("OOH" => $additionArray[11]['OOH']),
                        array("BTL" => $additionArray[12]['BTL']),
                        array("Total" => $additionArray[13]['Total']),
                    );
                    $additionArray = $this->_initializeArray($gettingKeysResult);
                }
                $i++;
            }
        }
        return $finalResult;
    }

    /**
     * get version data
     */
    public function getversion_post(){
        //geting http data from php:..input to php post variable
        $_POST = json_decode(file_get_contents("php://input"), true);
        $id = $this->input->post('id');
        //select query
        $selectVersionRecord = $this->api_model->select('data_version', 'value, timestamp', array('data_id' => $id), null, null, false, "id DESC");
        if($selectVersionRecord){
            //select query
            $geBrandQuery = "SELECT `categories`.`name` AS 'brand', `advertisement_media`.`name` AS advertisement_media FROM `data`
LEFT JOIN `categories` ON `data`.`categories` = `categories`.`id` 
LEFT JOIN `advertisement_media` ON `data`.`advertisement_media` = `advertisement_media`.`id` WHERE `data`.`id` = $id";
            $BrandResult = $this->api_model->custom_query($geBrandQuery);
            $message = array("status" => 1, "data" => $selectVersionRecord, "brand" => $BrandResult);
        }else{
            $message = array("status" => 0, "data" => array());
        }
        //sending response to browser
        $this->set_response($message, REST_Controller::HTTP_OK);
    }
}
