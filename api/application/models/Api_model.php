<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    /**
     * Insert Query
     *
     * @param type $table_name
     * @param type $post_data
     * @return type
     */
    public function insert($table_name, $post_data, $is_udid)
    {
        if ($is_udid) {
            $this->db->set('udid', 'UUID()', FALSE);
        }
        $this->db->insert($table_name, $post_data);
        return $this->db->insert_id();
    }

    /**
     * Insert Query
     *
     * @param type $table_name
     * @param type $post_data
     * @return type
     */
    public function insert_noid($table_name, $post_data)
    {
        $this->db->insert($table_name, $post_data);
        return $this->db->insert_id();
    }

    /**
     * Batch Upload
     *
     * @param $table_name
     * @param $post_data
     */
    public function insert_batch($table_name, $post_data)
    {
        $this->db->insert_batch($table_name, $post_data);
        return $this->db->insert_id();
    }

    /**
     * Select query
     *
     * @param type $table_name
     * @param type $select
     * @param type $where
     * @param type $offset
     * @param type $limit
     * @param $is_row
     * @return
     */
    public function select($table_name, $select, $where, $offset, $limit, $is_row, $order_by)
    {
        if (isset($select)) {
            $this->db->select($select);
        }
        if (isset($where)) {
            $this->db->where($where);
        }
        if (isset($order_by)) {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get($table_name, $limit, $offset);
        if ($is_row) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    /**
     * Select query with false
     *
     * @param type $table_name
     * @param type $select
     * @param $is_function
     * @param type $where
     * @param type $offset
     * @param type $limit
     * @param $is_row
     * @return
     */
    public function select_with_false($table_name, $select, $is_function, $where, $offset, $limit, $is_row)
    {
        if (isset($select)) {
            $this->db->select($select, $is_function);
        }
        if (isset($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get($table_name, $limit, $offset);
        if ($is_row) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    /**
     * Update Query
     *
     * @param type $table_name
     * @param type $data
     * @param type $cond
     * @return type
     */
    public function update($table_name, $data, $cond)
    {
        $this->db->trans_start();
        $this->db->where($cond);
        $this->db->update($table_name, $data);
        $affected = $this->db->affected_rows();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Update Query with mysql function support
     *
     * @param $table_name "Table Name"
     * @param $field_name "Field Name"
     * @param $data
     * @param $cond
     * @return mixed
     */
    public function update_with_false($table_name, $field_name, $data, $cond)
    {
        $this->db->set($field_name, $data, false);
        $this->db->where($cond);
        $this->db->update($table_name);
        $affected = $this->db->affected_rows();
        return $affected;
    }

    /**
     * Custom Query
     *
     * @param type $query
     * @return type
     */
    public function custom_query($query)
    {
        $query = $this->db->query($query);

        return $query->result_array();
    }


    public function custom_query_num($query)
    {
        $query = $this->db->query($query);

        return $query->num_rows();
    }


    public function custom_query_update($query)
    {
        $query = $this->db->query($query);
        if ($query) {
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * Get num_rows
     *
     * @param type $table
     * @param type $data
     * @return boolean
     */
    public function get_num_rows($table, $data)
    {
        if ($this->db->get_where($table, $data)->num_rows() === 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function delete($table, $where)
    {
        if ($this->get_num_rows($table, $where)) {
            $this->db->where($where);
            $this->db->delete($table);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function custom_query_delete($sql)
    {
        $query = $this->db->query($sql);
        if ($query) {
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }


    /**
     * Custom Query response single row
     *
     * @param type $query
     * @return type
     */
    public function custom_query_row($query)
    {
        $query = $this->db->query($query);

        return $query->row_array();
    }

    /*
     * alter a table
     * */
    public function alter_table($query)
    {
        $query = $this->db->query($query);
    }

    public function __destruct()
    {
        $this->db->close();
    }

    /**
     * Custom Query Return Object for CSV Generation
     *
     * @param type $query
     * @return type
     */
    public function query_obj($query)
    {
        return $this->db->query($query);
    }

    /**
     *
     * setSession function
     * @param $userId
     * @param $sessionId
     */
    public function setSession($userId, $sessionId)
    {
        $oldSessionId = $this->db->select('session_id')
            ->where(array('email' => $userId))
            ->get('user')
            ->row('session_id');
        if (empty($oldSessionId)) {
            //Map new session to the user Id
            $this->db->set('session_id', $sessionId);
            $this->db->where('email', $userId);
            $this->db->update('user');
        } else {
            //Destroy session which was mapped to previous user
            $this->db->where('id', $oldSessionId);
            $this->db->delete('ci_sessions');

            //Map new session to the user Id
            $this->db->set('session_id', $sessionId);
            $this->db->where('email', $userId);
            $this->db->update('user');
        }
        return true;
    }
}
