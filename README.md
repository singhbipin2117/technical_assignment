# Assignment PHP

Php and Angular 4 application for displaying Data in excel like ro and column

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

1) Apache
2) Mysql database
3) Php
4) Angular 4

Follow belo link to install all the aboe prerequisit.

[Install Apache2, Php, Mysql](https://websiteforstudents.com/installing-phpmyadmin-apache2-mysql-php-ubuntu-17-04-17-10/)


### Installing
##### Mysql server and mysql client
Instal mysql-server and mysql-client on ubuntu

```
sudo apt-get update
sudo apt-get install mysql-server mysql-client
```
##### Apache2
Instal apache2 on ubuntu

```
sudo apt-get install apache2
```

Configure apache2 

```
sudo nano /etc/apache2/conf-enabled/security.conf
```
Change the "prod" line below and save the file.

```
# Set to one of:  Full | OS | Minimal | Minor | Major | Prod
# where Full conveys the most information, and Prod the least.
#ServerTokens Minimal
ServerTokens Prod
#ServerTokens Full
```
Next, configure the default DirectoryIndex directives.
```
sudo nano /etc/apache2/mods-enabled/dir.conf
```
Then change the index.php line below and save.

```
<IfModule mod_dir.c>
DirectoryIndex index.html index.htm index.php
</IfModule>
```

##### PHP
Instal php on ubuntu

```
sudo apt-get install php php-cgi libapache2-mod-php php-common php-pear php-mbstring
```
##### Configure apache2 to use php
After install PHP and other related scripts, run the commands below to enable Apache2 to use PHP.

```
sudo a2enconf php7.1-cgi
```
Reload Apache2
```
sudo systemctl reload apache2
```


## Built With

* [Codeigniter](https://codeigniter.com/) - The web framework used
* [Google map](https://cloud.google.com/maps-platform/) - Google map
* [Bootstrap 3](hhttps://getbootstrap.com/docs/3.3/) - Ui framework

## Authors

* **Bipin Singh**
